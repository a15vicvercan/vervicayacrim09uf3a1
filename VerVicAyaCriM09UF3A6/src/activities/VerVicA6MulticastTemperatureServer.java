package activities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class VerVicA6MulticastTemperatureServer {

	public static void main(String[] args) throws Exception {
		
		//Get the multicast IP address object of the group from the multicast IP in string format.
        InetAddress group = InetAddress.getByName(args[1]);
        
        // Aquest és el port del grup (del MulticastSocket). Els packets s'han d'enviar a aquest port.
        int numPort = Integer.parseInt(args[0]);
        
        //Open an UDP socket. Aquest no és el port del grup, és un port diferent. (Mireu l'exemple de l'activitat 3. S'utilitzen dos ports, el 4445 i 4446).  
        DatagramSocket socket = new DatagramSocket();
        
        // Create an instance of the temperature probe provided in the activity (TemperatureProbe.java)
        VerVicA6TemperatureProbe tp = new VerVicA6TemperatureProbe(5);

        // Create an ScheduledExecutorService with a single thread (UF2):
        ScheduledExecutorService schExService = Executors.newSingleThreadScheduledExecutor();
        
        // Falta el bucle! El mètode schedule, només executa la tasca una vegada. Per tant només s'envia un paquet. Si no arriba es perd.
        final long NANOSEC_PER_SEC = 1000l*1000*1000;

        long startTime = System.nanoTime();
        while ((System.nanoTime()-startTime)< 10*60*NANOSEC_PER_SEC) {
        
	        //Create an empty buffer (byte[])
	        byte[] buf = new byte[256];
	        
	        // Run the probe task after 5 seconds.
	        ScheduledFuture<byte[]> scheduledFuture = schExService.schedule(tp, 5, TimeUnit.SECONDS);
          
        	// Get data from the probe after the 5 seconds. ( // 12. Modify the empty buffer with the information to be sent to the client in the packet )
            buf = scheduledFuture.get();
         
            // 14. Create a data packet to be sent to the group (IP and port indicated in the arguments. The port is the same used by the MulticastSocket in the client.
            DatagramPacket packet = new DatagramPacket(buf, buf.length, group, numPort);
            
            // 15. Send the packet to the group.
            socket.send(packet);
        	
        }

        //Close the socket
    	socket.close();    	
    	
	}
	
}
