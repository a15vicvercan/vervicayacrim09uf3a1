package activities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class AyaCriA6TemperatureClient {
	public static void main(String[] args) throws IOException {
    	
    	//Port => 1234
		int numPort = Integer.parseInt(args[0]);
		
    	//Open an UDP Multicast socket
    	// El port del MulticastSocket ha de ser "numPort": el mateix indicat en el server. El server envia els paquets a la IP del grup i un port.  
        MulticastSocket socket = new MulticastSocket(numPort);
        
        // NEW in this activity: Indicate the time to wait for data to receive → 5s at most. After this time a TIME OUT occurs
        socket.setSoTimeout(5000);
        
        //Get the multicast IP address object of the group from the multicast IP in string format.
        InetAddress address = InetAddress.getByName(args[1]);
        
        //Join the group
        socket.joinGroup(address);

        DatagramPacket packet;
        int numTimeOut = 0;
        
        do {
        	//Create an empty buffer (byte[]).
        	byte[] buf = new byte[256];
        	//Create a data packet with an empty buffer to receive information from the server (sent to the group).
        	packet = new DatagramPacket(buf, buf.length);
        	//Receive a packet sent to the group from the server.
            socket.receive(packet);
            //Get the received information (byte[]) from the packet.
            String received = new String(packet.getData(), 0, packet.getLength());        
            if (received == null) numTimeOut++;
            else System.out.println("The actual temperature is: " + received + " degrees.");
            
            if (numTimeOut == 5) System.out.println("A timeout occurred.");
	    } while (numTimeOut != 5);
        
        //Leave the group.
        socket.leaveGroup(address);
        //Close the socket.
        socket.close();
    }
}
