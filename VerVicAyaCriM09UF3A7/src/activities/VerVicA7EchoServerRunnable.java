package activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class VerVicA7EchoServerRunnable implements Runnable{

	private Socket clientSocket = null;
	
	public VerVicA7EchoServerRunnable(Socket clientSocket) {
		// TODO Auto-generated constructor stub
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		try {
			// Open the output stream to the client socket.
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			//Open the input stream of the client socket.
	        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	        
	        String inputLine;
	        //Read from the client socket input stream.
	        while ((inputLine = in.readLine()) != null) {
	        	//Server processing.
	        	
	        	//Write to the client socket output stream.
	            out.println(inputLine);
	            
	        }
	        
	        System.out.println("SALE UN CLIENTE");
	        
	        //Close the input stream of the client socket. 
	    	if(in != null) in.close();
			//Close the output stream of the client socket. 
	    	if(out != null) out.close();
	    	//Close the client socket. 
	    	clientSocket.close(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}     
		
		
	}
	
}
