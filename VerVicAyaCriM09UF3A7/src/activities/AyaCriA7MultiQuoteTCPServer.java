package activities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;


public class AyaCriA7MultiQuoteTCPServer {
	
	//private static BufferedReader in = null;

	public static void main(String[] args) throws NumberFormatException, IOException {
		

		if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        ServerSocket serverSocket = null;
        
        try {
        	// Open the server socket to listen on port [4321].
            serverSocket = new ServerSocket(Integer.parseInt(args[0]));
            InetAddress inetAddress = InetAddress.getLocalHost();
            
            while (true) {
            	// Open a client socket.
                Socket clientSocket = serverSocket.accept();
                
                new Thread(new AyaCriA7MultiQuoteServerRunnable(clientSocket)).start();
            }
        	
        } catch (FileNotFoundException e) {
            System.err.println("Could not open quote file. Serving time instead.");
        } finally {
        	// Close the server socket.
        	serverSocket.close();
		}	
	}

}
