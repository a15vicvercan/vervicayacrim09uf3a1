package activities;
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

import java.io.*;
import java.net.*;
import java.util.*;

public class VerVicA3MulticastClient {

    public static void main(String[] args) throws IOException {
    	
    	int numPort = Integer.parseInt(args[0]);
    	
    	//Port => 4446
    	//Open an UDP Multicast socket
        MulticastSocket socket = new MulticastSocket(numPort);
        //Get the multicast IP address object of the group from the multicast IP in string format.
        InetAddress address = InetAddress.getByName(args[1]);
        //Join the group
        socket.joinGroup(address);

        DatagramPacket packet;
    
        Scanner input = new Scanner(System.in);
        String respuesta = "";
        do {
        	//Create an empty buffer (byte[]).
        	byte[] buf = new byte[256];
        	//Create a data packet with an empty buffer to receive information from the server (sent to the group).
        	packet = new DatagramPacket(buf, buf.length);
        	//Receive a packet sent to the group from the server.
            socket.receive(packet);
            //Get the received information (byte[]) from the packet.
            String received = new String(packet.getData(), 0, packet.getLength());
            System.out.println("Quote of the Moment: " + received);
	        System.out.print("Do you want to get another quote (Y|N)? ");
	        respuesta = input.nextLine();
	        if (respuesta.equals("N")) System.out.println("Bye!");
	    } while (!respuesta.equals("N"));
        //Leave the group.
        socket.leaveGroup(address);
        //Close the socket.
        socket.close();
    }

}
