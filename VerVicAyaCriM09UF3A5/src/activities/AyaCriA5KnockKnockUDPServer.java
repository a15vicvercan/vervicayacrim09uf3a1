package activities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class AyaCriA5KnockKnockUDPServer {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
        
        // Obrir un socket UDP en el port 8080 (Open an UDP socket).
        DatagramSocket socket = new DatagramSocket(8080);
        
        // Crear una instància del KnockKnockProtocol → kkp.
        KnockKnockProtocol kkp = new KnockKnockProtocol();
        
	     // Rebre el primer paquet del client:
	     // 1. Create a data packet with an empty buffer to receive information from the client.
        byte[] buf = new byte[256];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        
        // 2. Receive a packet from the client.
        socket.receive(packet);
        
        // 3. Get the received information (byte[]) from the packet → inputLine
        String received = new String(packet.getData(), 0, packet.getLength());
        
        // 4. Process the response: outputLine = kkp.processInput(null); // null→ pot ser la info que s’ha rebut en el paquet (inputLine).
        String outputLine = kkp.processInput(received);
        
        do {
        
	        // Enviar resposta al client:
	        // 1. Obtenir la IP del client del paquet rebut. (Get the IP address object of the client from the packet) → clientAdress
	        InetAddress address = packet.getAddress();
	        
	        // 2. Obtenir el port del client del paquet rebut. (Get the port of the client, which is NOT the same used by the server: random one)
	        int port = packet.getPort();
	        
	        // 3. Crear un array de bytes amb la cadena a enviar «outputLine».És a dir, passar a bytes la cadena -> outputLineBytes.
	        byte[] buf1 = outputLine.getBytes();
	        
	        // 4. Crear un paquet pel client amb IP «clientAdress» dirigit al port «clientPort» amb les dades «outputLineBytes». (Create a data packet to be sent to the client with IP «clientAdress» which is listening on port «clientPort»).
	        DatagramPacket packet2 = new DatagramPacket(buf1, buf1.length, address, port);
	        
	        // 5. Enviar el paquet al client (Send the packet to the client).
	        socket.send(packet2);
	        
	        
	        // Rebre del client:
	        // 1. Create a data packet with an empty buffer to receive information from the client.
	        byte[] buf2 = new byte[256];
	        DatagramPacket packet1 = new DatagramPacket(buf2, buf2.length, address, port);
	        
	        // 2. Receive a packet from the client.
	        socket.receive(packet1);
	        
	        // 3. Get the received information (byte[]) from the packet.
	        String received2 = new String(packet1.getData(), 0, packet1.getLength());
	        
	        // 4. Process the response: outputLine = kkp.processInput();
	        outputLine = kkp.processInput(received2);
	        
        
        }while(!outputLine.equals("Bye."));
        
        // Tancar el socket (Close the socket).
        socket.close();

	}

}
