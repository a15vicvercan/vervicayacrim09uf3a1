package activities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class VerVicA5KnockKnockUDPClient {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String received;
		String respuesta1;

		if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }
		
		// Crear un objecte IP a partir d’un String amb l’IP del server (Get the IP address object of the server from the host name or host IP in string format) → address
		InetAddress address = InetAddress.getByName(args[0]);
		
		// Obtenir el port a partir d’un String (dels arguments del main) → port
        int portNumber = Integer.parseInt(args[1]);
        
        // Obrir un socket UDP (Open an UDP socket).
        DatagramSocket socket = new DatagramSocket();
        
        // Obrir un flux de dades per llegir de la consola.
        Scanner input = new Scanner(System.in);
        String respuesta = "";
        
	     // Enviar primer paquet buit al servidor (Send first request):
	     // 1. Crear un array de dades byte buit. (Create an empty buffer byte[]).
        byte[] buf = new byte[256];
        
        // 2. Crear un paquet pel servidor amb IP «address» dirigit al port «8080». Les primeres dades poden ser buides o un missatge específic. (Create a data packet to be sent to the client with IP «address» which is listening on port 8080).
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, portNumber);
        
        // 3. Enviar el paquet al servidor (Send the packet to the server).
        socket.send(packet);
        
        do {
        
	        // Obtenir la resposta del servidor (Get response)
	        // 1. Create a data packet with an empty buffer to receive information from the server.
        	byte[] buf2 = new byte[256];
	        packet = new DatagramPacket(buf2, buf2.length);
	        
	        // 2. Receive a packet from the server.
	        socket.receive(packet);
	        
	        // 3. Get the received information (byte[]) from the packet.
	        received = new String(packet.getData(), 0, packet.getLength());
	        
	        // 4. Print the response.
	        System.out.println(received);
	        
	        // Enviar al servidor
	        // 0. Llegir una línia de la consola → dataString
	        System.out.print("Client: ");
	        respuesta1 = input.nextLine();
	        
	        // 1. Crear un array de bytes amb la cadena llegida. És a dir, passar a bytes la cadena llegida → dataBytes
	        byte[] buf1 = respuesta1.getBytes();
	        
	        // 2. Crear un paquet pel servidor amb IP «address» dirigit al port «8080» amb les dades «dataBytes».
	        DatagramPacket packet1 = new DatagramPacket(buf1, buf1.length, address, portNumber);
	        
	        // 3. Enviar el paquet (Send the packet to the server).
	        socket.send(packet1);
        
        }while(!respuesta1.equals("n"));
        
        // Tancar el socket (Close the socket).
        input.close();
        socket.close();
		
	}

}
