package activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class AyaCriA4QuoteTCPClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		
		Scanner input = new Scanner(System.in);
		// TODO Auto-generated method stub

		if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        
        //Open a client socket
        Socket clientSocket = new Socket(hostName, portNumber);
    	//Open the output stream to the client socket
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        //Open the input stream of the client socket
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));   

        try {
        	//BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            String userInput;
            
            System.out.println("Do you want to get a quote (Y|N)?");
            
            //Read from the client socket input stream.
            while ((userInput = input.nextLine()) != null) {
            	
            	if (userInput.equals("N")) {
            		System.out.println("Bye.");
                    break;
                }
            	else if (userInput.equals("Y")) {
                	System.out.println("Which one? Write a number.");
                	int num = input.nextInt();
                	out.println(num);
                	System.out.println(in.readLine());	
                	System.out.println("Do you want to get a quote (Y|N)?");
                	input.nextLine();
            	}
            	else {
            		System.out.println("Answer (Y|N), please. Do you want to get a quote (Y|N)?");
            	}       
            }
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
        	//Exception thrown to indicate that the IP address of a host (the server) could not be determined
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        } finally {
        	if (input != null) input.close();
        	//Close the input stream of the client socket
        	if(in != null) in.close();
        	//Close the output stream of the client socket
			if(out != null) out.close();
			
			//Close the client socket
			clientSocket.close();
        }
        
	}

}
