package activities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class VerVicA4QuoteTCPServer {
	
	private static BufferedReader in = null;

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub

		if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        // Open the server socket to listen on port [4321].
        ServerSocket serverSocket = new ServerSocket(Integer.parseInt(args[0]));
        InetAddress inetAddress = InetAddress.getLocalHost();
        // Open a client socket.
        Socket clientSocket = serverSocket.accept();
        
        //Open the output stream to the client socket. 
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        //Open the input stream of the client socket
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        
        try {
        	String inputLine, outputLine;
        	
        	//Read from the client socket input stream.
            while ((inputLine = in.readLine()) != null) {
                	String line = Files.readAllLines(Paths.get("one-liners.txt")).get(Integer.parseInt(inputLine) - 1);
                	out.println(line);
            }     

        } catch (FileNotFoundException e) {
            System.err.println("Could not open quote file. Serving time instead.");
        } finally {
        	//Close the input stream of the client socket. 
        	if(in != null) in.close();
			//Close the output stream of the client socket. 
        	if(out != null) out.close();
        	//Close the client socket. 
        	clientSocket.close(); 
        	// Close the server socket.
        	serverSocket.close();
        }
		
	}

}
